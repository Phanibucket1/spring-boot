package com.example.ribbion.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRibbionUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRibbionUserApplication.class, args);
	}

}
