package com.example.jooq.service;

import java.util.List;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tej.JooQDemo.jooq.sample.model.Tables;
import com.tej.JooQDemo.jooq.sample.model.tables.pojos.Students;

@Service
public class StudentService {

	@Autowired
	private DSLContext dslContext;

	public void insert(Students students) {
		System.out.println("calleed*******");
		dslContext.insertInto(Tables.STUDENTS, Tables.STUDENTS.ID, Tables.STUDENTS.NAME, Tables.STUDENTS.AGE)
				.values(students.getId(), students.getName(), students.getAge()).execute();
		System.out.println("ended*******");
	}

	public List<Students> getBooks() {
		return dslContext.selectFrom(Tables.STUDENTS).fetchInto(Students.class);
	}

}
