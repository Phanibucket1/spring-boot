package com.example.jooq;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.jooq.service.StudentService;
import com.tej.JooQDemo.jooq.sample.model.tables.pojos.Students;

@SpringBootApplication
@RestController
@RequestMapping("/students")
public class SpringBootJooqApplication {

	@Autowired
	private StudentService service;

	@PostMapping("/add")
	public String addStudent(@RequestBody Students students) {
		service.insert(students);
		return "Students added successfully....";
	}

	@GetMapping("/getbooks")
	public List<Students> getStudents() {
		return service.getBooks();
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJooqApplication.class, args);
	}

}
