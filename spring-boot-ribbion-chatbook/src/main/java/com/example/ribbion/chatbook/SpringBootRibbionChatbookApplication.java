package com.example.ribbion.chatbook;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RequestMapping("/chat-book-application")
public class SpringBootRibbionChatbookApplication {

	@Value("${server.port}")
	String port;

	@GetMapping("/chat")
	public String chatNow() {
		return "app running on port " + port;
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRibbionChatbookApplication.class, args);
	}

}
