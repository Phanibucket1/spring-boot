package com.example.feign.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableFeignClients
@EnableDiscoveryClient
public class SpringBootFeignClientApplication {
	
	@Autowired
	private UserClient client;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootFeignClientApplication.class, args);
	}
	
	@RequestMapping("/feign-client")
	public String feignClientTest() {
		return client.getResponse();
	}

}
