package com.example.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "user-client", url = "http://PAYMENT-SERVICE/")
public interface UserClient {
	
	@GetMapping("/payment-provider/pay/200")
	public String getResponse();

}
