package com.example.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;

@Configuration
public class CacheConfig {
	
	@Bean
	public Config configure() {
		return new Config().setInstanceName("hazlecase-instance").
				addMapConfig(new MapConfig().setName("userCache"));
	}
}
