package com.example.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.web.dao.BookRepository;
import com.example.web.models.Book;

@Service
public class BookService {

	@Autowired
	private BookRepository repository;

	public String saveBook(Book book) {
		repository.save(book);
		return "Book Saved with ID" + book.getBookId();
	}

	@Cacheable(value = "userCache", key = "#bookid", unless = "#result==null")
	public Book getBook(int bookid) {
		return repository.findById(bookid).get();
	}

	@CacheEvict(value = "userCache")
	public List<Book> removeBook(int bookid) {
		repository.deleteById(bookid);
		return repository.findAll();
	}

	@Cacheable(cacheNames = { "userCache" })
	public List<Book> findAllBooks() {
		System.out.println("First time hit.....");
		return repository.findAll();
	}
}
