package com.example.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SpringBootCacheHazlecastApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCacheHazlecastApplication.class, args);
	}

}
