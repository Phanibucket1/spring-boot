package com.example.jwt.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.jwt.security.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByUserName(String username);

}
