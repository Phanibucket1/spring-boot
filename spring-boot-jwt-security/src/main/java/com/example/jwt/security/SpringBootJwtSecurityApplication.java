package com.example.jwt.security;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.jwt.security.entity.User;
import com.example.jwt.security.repository.UserRepository;

@SpringBootApplication
public class SpringBootJwtSecurityApplication {
	
	@Autowired
	private UserRepository userRepository;

	@PostConstruct
	public void initUsers() {
		System.out.println("Post construct called");
		List<User> users = Stream.of(
				new User(1, "Phani", "Phani", "phanichowdary82@gmail.com"),
				new User(2, "Sai", "Sai", "Sai01@gmail.com"))
				.collect(Collectors.toList());
		userRepository.saveAll(users);
		System.out.println("Post construct ended");
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJwtSecurityApplication.class, args);
	}

}
