package com.example.jwt.security.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.jwt.security.entity.User;
import com.example.jwt.security.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("UserDetails loadUserByUsername "+ username);
		List<User> findAll = userRepository.findAll();
		System.out.println("All users count "+ findAll.size() + " "+ findAll.toString());
		System.out.println("User name found "+ username);
		User user = userRepository.findByUserName(username);
		System.out.println("User "+ user);
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
				new ArrayList<>());
	}

}
