package com.example.jwt.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.jwt.security.entity.AuthRequest;
import com.example.jwt.security.util.JwtUtil;

@RestController
public class WelcomeController {

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	@GetMapping("/")
	public String welcome() {
		return "welcome";
	}

	@PostMapping("/generateToken")
	public String generateToken(@RequestBody AuthRequest request) throws Exception {
		System.out.println("generateToken calledd");
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(request.getUserName(), request.getPassword()));
			return jwtUtil.generateToken(request.getUserName());
		} catch (Exception e) {
			throw new Exception("Invalid username/password");
		}
	}

}
