package com.example.eureka.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringBootEurekaService1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEurekaService1Application.class, args);
	}

}
