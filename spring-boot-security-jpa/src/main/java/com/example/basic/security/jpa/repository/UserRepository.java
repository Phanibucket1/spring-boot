package com.example.basic.security.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.basic.security.jpa.models.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public User findByUserName(String username);

}
