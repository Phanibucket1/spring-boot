package com.example.basic.security.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.basic.security.jpa.models.User;
import com.example.basic.security.jpa.repository.UserRepository;

@RestController
@RequestMapping("/secure/rest")
public class AdminController {

	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public AdminController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping("/admin/add")
	public String addUserByAdmin(@RequestBody User user) {
		String pass = user.getPassword();
		String encPass = bCryptPasswordEncoder.encode(pass);
		user.setPassword(encPass);
		userRepository.save(user);
		return "user added successfully";
	}
}
