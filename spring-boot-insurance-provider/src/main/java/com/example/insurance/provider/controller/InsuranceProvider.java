package com.example.insurance.provider.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/insurance-service")
public class InsuranceProvider {

	@GetMapping("/getUpdatedPlans")
	public List<String> getAllPlans(){
		return Stream.of("Gold","platinum").collect(Collectors.toList());
	}

}
