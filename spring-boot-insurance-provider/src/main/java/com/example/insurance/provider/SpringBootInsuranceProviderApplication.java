package com.example.insurance.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootInsuranceProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootInsuranceProviderApplication.class, args);
	}

}
