package com.example.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.web.dao.BookRepository;
import com.example.web.models.Book;

@Service
public class BookService {

	@Autowired
	private BookRepository repository;

	public String saveBook(Book book) {
		repository.save(book);
		return "Book Saved with ID" + book.getBookId();
	}

	public Book getBook(int bookid) {
		return repository.findById(bookid).get();
	}

	public List<Book> removeBook(int bookid) {
		repository.deleteById(bookid);
		return repository.findAll();
	}
}
