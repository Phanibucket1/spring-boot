package com.example.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.web.models.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {

}
