package com.example.schedular.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.schedular.model.User;

public interface UserDao extends JpaRepository<User, Integer>{

}
