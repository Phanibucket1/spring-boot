package com.example.schedular.service;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.schedular.dao.UserDao;
import com.example.schedular.model.User;

@Service
public class UserService {
	
	Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserDao dao;

	@Scheduled(fixedRate = 5000)
	public void add2DBJob() {
		User user = new User();
		user.setName("User" + new Random().nextInt(374483));
		dao.save(user);
		System.out.println("User added successfully.."+ new Date().toString());
		log.info("User added successfully {}", new Date().toString());
	}
	
	@Scheduled(cron = "*/15 * * * * *")
	public void fetchDBJob() {
		List<User> users = dao.findAll();
		System.out.println("fetch call and users count.."+ users.size());
		log.info("fetch call and users count {}", users.size());
	}
	
}
