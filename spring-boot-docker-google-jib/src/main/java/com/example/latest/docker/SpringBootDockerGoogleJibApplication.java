package com.example.latest.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootDockerGoogleJibApplication {

	
	@GetMapping("/message")
	public String message() {
		return "Message from docker google JIB plugin";
	}
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootDockerGoogleJibApplication.class, args);
	}

}
