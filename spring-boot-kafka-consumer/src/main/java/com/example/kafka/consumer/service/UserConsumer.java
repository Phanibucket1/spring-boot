package com.example.kafka.consumer.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.kafka.publisher.config.User;

@Service
public class UserConsumer {

	@KafkaListener(topics = { "message-post-topic" })
	public void consume(User user) {
		System.out.println("user details " + user.getId() + " name:: " + user.getName());
	}

}
