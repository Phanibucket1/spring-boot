package com.example.basic.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class SpringBootBasicSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBasicSecurityApplication.class, args);
	}

}
