package com.example.jsocdocs.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.jsocdocs.models.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {

}
