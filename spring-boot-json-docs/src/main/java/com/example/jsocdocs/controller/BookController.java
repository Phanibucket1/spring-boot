package com.example.jsocdocs.controller;

import java.util.List;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.pojo.ApiStage;
import org.jsondoc.core.pojo.ApiVisibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.jsocdocs.models.Book;
import com.example.jsocdocs.service.BookService;

@RestController
@Api(name = "Book System", description = "Book Info", group = "Book Group", visibility = ApiVisibility.PUBLIC, stage = ApiStage.ALPHA)
public class BookController {

	@Autowired
	private BookService service;

	@RequestMapping(value = "/saveBook", method = RequestMethod.POST)
	@ApiMethod(description = "Save Book information")
	public String saveBook(@RequestBody Book book) {
		return service.saveBook(book);
	}

	@RequestMapping(value = "/getBook/{bookId}", method = RequestMethod.GET)
	@ApiMethod(description = "Search book by ID", path = {"bookId"})
	public Book getBook(@PathVariable @ApiPathParam(description = "provide the id", name = "bookId") int bookId) {
		return service.getBook(bookId);
	}

	@RequestMapping(value = "/deletebook/{bookId}", method = RequestMethod.GET)
	@ApiMethod(description = "Delete book by ID", path = {""})
	public List<Book> deleteBook(@PathVariable @ApiPathParam(description = "provide the id", name = "bookId") int bookId) {
		return service.removeBook(bookId);
	}

}
