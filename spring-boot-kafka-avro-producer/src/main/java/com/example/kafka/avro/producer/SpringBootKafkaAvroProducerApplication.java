package com.example.kafka.avro.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootKafkaAvroProducerApplication {
	
	@Autowired
	private KafkaTemplate<String, Order> kafkaTemplate;
	
	@PostMapping("/publishOrder")
	public String publishOrder(Order order) {
		kafkaTemplate.send("message-post-topic", order);
		return "Order Posted Successfully....";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootKafkaAvroProducerApplication.class, args);
	}

}
