package com.example.kafka.publisher;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.publisher.config.User;

@SpringBootApplication
@RestController
public class SpringBootKafkaPublisherApplication {

	@Autowired
	private KafkaTemplate<String, Object> kafkaTemplate;

	@Autowired
	private KafkaProducer<String, User> kafkaProducer;

	private String topic_name = "message-post-topic";

	@GetMapping("/publish/{message}")
	public String publishMessage(@PathVariable String message) {
		kafkaTemplate.send(topic_name, "Hi " + message + " welcome to java Techie");
		return "Data Pulished successfully";
	}

	@GetMapping("/publishJson")
	public String publishJson() {
		kafkaTemplate.send(topic_name, new User(1, "Phani", new String[] { "Hyd", "Chennai" }));
		return "Data Pulished successfully";
	}

	@GetMapping("/publishJsonWithProducer")
	public String publishJsonWithProducer() {
		kafkaProducer.send(new ProducerRecord<String, User>(topic_name,
				new User(1, "from Kafka Producer", new String[] { "Hyd", "Chennai" })));
		return "Data Pulished successfully";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootKafkaPublisherApplication.class, args);
	}

}
