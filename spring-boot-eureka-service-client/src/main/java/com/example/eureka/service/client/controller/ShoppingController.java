package com.example.eureka.service.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ShoppingController {

	@Autowired
	RestTemplate restTemplate;

	@GetMapping("/invokepayment/{price}")
	public String invokePaymentService(@PathVariable int price) {

		// String url ="http://localhost:9090/payment-provider/pay/"+price;
		String url = "http://PAYMENT-SERVICE/payment-provider/pay/" + price;
		String response = restTemplate.getForObject(url, String.class);
		return response;
	}

}
