package com.example.config.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
@RefreshScope
public class SpringBootConfigCleintApplication {
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootConfigCleintApplication.class, args);
	}
	
	@Autowired
	@Lazy
	private RestTemplate restTemplate;
	
	@Value("${insurance.provider.url}")
	private String url;
	
	@GetMapping("/getPlans")
	public List<String> getAllPlans(){
		return restTemplate.getForObject(url, List.class);
	}

}
